const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]

let productsArray = Object.entries(products[0]);

let productsWithPriceInNumber = productsArray.reduce((acc, curr) => {
    if (typeof (curr[1].price) === 'string') {
        curr[1].price = parseInt(curr[1].price
            .replace('$', ''));
        if (curr[1].price > 65) {
            acc.push(curr);
        }
    } else {
        let nestedItem = (curr.flat());
        nestedItem.shift();
        let finalItems = nestedItem.filter((item) => {
            let key = Object.keys(item);
            let price = parseInt((item[key].price)
                .replace('$', ''));
            if (price > 65) {
                return item;
            }
        });
        acc.push(finalItems);
    }
    return acc;
}, []);

console.log(productsWithPriceInNumber.flat());

